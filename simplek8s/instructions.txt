kubectl apply -f client-pod.yml
kubectl apply -f client-nodeport.yml
# instead of client-pod.yml
kubectl apply -f client-deployment.yml

# force deployment using an updated image (whithout changes in the client-deployment.yml file)
kubectl set image deployment/client-deployment client=stephengrider/multi-client:v5